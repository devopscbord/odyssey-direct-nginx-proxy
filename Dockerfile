# Download base image
FROM  fedora

# Start with updated repos
RUN dnf -y update && dnf clean all

# install needed dependencies
RUN dnf -y install nginx openssl git unzip && dnf clean all

# Add local files and run build scripts
ADD ./install_easyrsa_certs.sh /etc/nginx/ssl/install_easyrsa_certs.sh
ADD ./host_map.txt /etc/nginx/host_map.txt
ADD ./nginx.conf /etc/nginx/nginx.conf
RUN /etc/nginx/ssl/install_easyrsa_certs.sh


# Identify the appropriate ports to expose, 80/443 likely
EXPOSE 80
EXPOSE 443


CMD [ "/usr/sbin/nginx" ]
