Use these for the bootstrap:
# Add to boot script: git clone git@github.com:fedora-cloud/Fedora-Dockerfiles.git
sudo dnf -y install dnf-plugins-core
sudo dnf config-manager --add-repo https://download.docker.com/linux/fedora/docker-ce.repo
sudo dnf install -y docker-ce docker-ce-cli containerd.io
sudo systemctl enable docker
sudo systemctl start docker
sudo docker run -d --name odyssey_proxy -p 80:443 devops/odyssey-direct-nginx-proxy
