home_dir="/home/root"
easy_rsa_git_repo="https://github.com/openvpn/easy-rsa.git"
easy_rsa_dir="/home/root/easy-rsa/easyrsa3"
easy_rsa_pki="$easy_rsa_dir/pki"
target_cert_dir="/etc/nginx/ssl/"
cn="Easy-RSA CA"


function git_clone(){
        args="$1"

        git clone "$args"

}

function gen_dhparam(){
        target_dir="$1"

        openssl dhparam -out "$target_dir/dhparam.pem"  2048

}

function update_local_rsa_vars(){
        cn="$1"

        echo "set_var EASYRSA_REQ_CN=\"$cn\""  >> ./vars
}

function gen_certs(){
        req_cn="$1"

        update_local_rsa_vars "$req_cn"
        ./easyrsa init-pki
        ./easyrsa --batch build-ca nopass

}

function install_certs(){
        target_dir="$1"

        mv ./ca.crt "$target_dir/easyrsa.crt"
        mv ./private/ca.key "$target_dir/easyrsa.key"

}

# make /home/root
mkdir --parents "$home_dir"
cd "$home_dir"

# generate the dhparam
gen_dhparam "$target_cert_dir"

# install easy-rsa
git_clone "$easy_rsa_git_repo"

# Move to easyrsa repo
cd "$easy_rsa_dir"

# generate the certificates with easy_rsa
gen_certs "$cn"

# Move to pki
cd "$easy_rsa_pki"

# Install certs in the new environment
install_certs "$target_cert_dir"
