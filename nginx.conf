user       www www;  ## Default: nobody
worker_processes  auto;  ## Default: 1
error_log  logs/error.log;
pid        logs/nginx.pid;
worker_rlimit_nofile 8192;

events {
  worker_connections  1024;  ## Default: 1024
}

http {
        max_ranges 1; # mitigation of CVE
        log_format pci_datacenter '$http_x_forwarded_for - $remote_addr - $remote_user [$request_time]sec [$time_local] "$request" $status $bytes_sent "$http_referer" "$http_user_agent" "$gzip_ratio" "$ssl_cipher" "$http_host" "$webserverdns"';
        access_log   logs/access.log  pci_datacenter;
        sendfile     on;
        tcp_nopush   on;
        # server_names_hash_bucket_size 128; # this seems to be required for some vhosts
                
        map $http_host $webserverdns {
               include /etc/nginx/host_map.txt 
        }

	# generated 2020-01-16, https://ssl-config.mozilla.org/#server=nginx&server-version=1.17.0&config=intermediate
	server {
	    listen 80 default_server;
	    listen [::]:80 default_server;

	    # redirect all HTTP requests to HTTPS with a 301 Moved Permanently response.
	    return 301 https://$host$request_uri;
	}

	server {
	    listen 443 ssl;
	    listen [::]:443 ssl;

	    # certs sent to the client in SERVER HELLO are concatenated in ssl_certificate
	    ssl_certificate /etc/nginx/ssl/easyrsa.crt;
	    ssl_certificate_key /etc/nginx/ssl/easyrsa.key;
	    ssl_session_timeout 1d;
	    ssl_session_cache shared:MozSSL:10m;  # about 40000 sessions
	    ssl_session_tickets off;

	    # curl https://ssl-config.mozilla.org/ffdhe2048.txt > /path/to/dhparam.pem
	    ssl_dhparam /etc/nginx/ssl/dhparam.pem;

	    # intermediate configuration
	    ssl_protocols TLSv1.2 TLSv1.3;
	    ssl_ciphers ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384;
	    ssl_prefer_server_ciphers off;

	    # HSTS (ngx_http_headers_module is required) (63072000 seconds)
	    # add_header Strict-Transport-Security "max-age=63072000" always;

	    # OCSP stapling
	    # ssl_stapling on;
	    # ssl_stapling_verify on;

	    # verify chain of trust of OCSP response using Root CA and Intermediate certs
	    # ssl_trusted_certificate /path/to/root_CA_cert_plus_intermediates;

	    # replace with the IP address of your resolver
	    resolver 169.254.169.253;
            
            # pass requests for dynamic content to rails/turbogears/zope, et al
            location / {
              proxy_pass      http://$webserverdns:8080/;

            }
      }

}

daemon off;
